﻿using UnityEngine;
using System.Collections;

public class Att_Compt : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(Att_Depart());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator Att_Depart()
    {
        while(Init_Niveau.wait)
        {
            yield return new WaitForSeconds(0.1f);
        }
        GetComponent<DestroyObjectAndAddPoint_M2>().enabled = true;
        yield return null;
    }
}
