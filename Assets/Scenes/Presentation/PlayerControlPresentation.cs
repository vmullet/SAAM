﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class PlayerControlPresentation : MonoBehaviour {
    public float h=1f;
    public float velocity;
    bool pr_slide = false;
    public bool jump = false;
    public bool run = true;
    public bool facingRight = true;

    public float moveForce = 365f;          // Amount of force added to move the player left and right.
    public float maxSpeed = 5f;             // The fastest the player can travel in the x axis.
    
    public float jumpForce = 180f;         // Amount of force added when the player jumps.
   
    private bool grounded = false;
    private bool wall = false;

    private Animator anim;
    private Transform groundCheck;
    private Transform wallCheck;
    private bool tournoyer = false;
    private new AudioSource audio;
    public AudioClip[] clips;
    private int cpt=0;
    public GameObject presentation_canvas;
    private bool fini=false;
    private int compt = 0;
    public GameObject[] letters;
	// Use this for initialization
	void Start () {
        groundCheck = transform.Find("groundCheck");
        wallCheck = transform.Find("wallCheck");
        anim = GetComponent<Animator>();
        StartCoroutine(tournex2());
        audio = GetComponent<AudioSource>();
        audio.Play();
    }
	
	// Update is called once per frame
	void Update () {
        
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        wall = Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("Wall"));
        
        if (grounded)
        {
            anim.SetTrigger("StopJumping");
            if (pr_slide)
            {
                if (velocity > 0.05 || velocity < -0.05)
                {
                      
                        anim.SetTrigger("Slide");
                        pr_slide = false;
                    
                }
            }
        }
        
    }

    void FixedUpdate()
    {
        
        // auto_move();
        //h= Input.GetAxis("Horizontal");
        presentation_screen();

        if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);
            velocity = GetComponent<Rigidbody2D>().velocity.x;
        }

        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
            velocity = GetComponent<Rigidbody2D>().velocity.x;
        }
        
        if (h > 0 && !facingRight)
           Tourne();
        else if (h < 0 && facingRight)
            Tourne();

        if (!run)
        {
            anim.SetTrigger("StopRunning");
        }
        if (jump)
        {
            anim.CrossFade("jump",0f);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
            jump = false;
            pr_slide = true;
        }
        
    }
        void Tourne()
        {

        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void auto_move()
    {
        if (grounded) //Si touche le sol
        {
            run = true; //booléen qui déclenche une force qui fait avancer le personnage
            
            anim.SetTrigger("Run"); //démarrage de l'animation
            if (transform.position.x > -1.5f && transform.position.x < 1.5f) //Saut prévu à cet emplacement
            {    
                    run = false; //Arret de courir
                    jump = true;   //Début du saut 
            }

            if (wall) //Si touche un mur
            {
                h = -h; //Inversion de la force gauche si droite ou droite si gauche
                Tourne(); //Le personnage tourne
            }
        }
        
    }

    public void presentation_screen()
    {
        if (grounded) //Si touche le sol
        {
            
            if (transform.position.x > 23f && transform.position.x < 30f&&!fini) //Saut prévu à cet emplacement
            {
               
                h = 0f;
                run = false; //Arret de courir
                tournoyer = true;
            }
            else
            {
                run = true; //booléen qui déclenche une force qui fait avancer le personnage

                anim.SetTrigger("Run"); //démarrage de l'animation
            }
        }
        

    }

    private IEnumerator tournex2()
    {
       
            while (!tournoyer)
            {
                yield return new WaitForSeconds(0.2f);
            }
        while (cpt < 4)
        {
            audio.loop = false;
           
            audio.clip = clips[0];
            audio.Play();
            Tourne();
            cpt++;
            yield return new WaitForSeconds(0.8f);

            audio.Play();
            Tourne();
            cpt++;
            yield return new WaitForSeconds(0.8f);
            yield return null;
        }
        yield return new WaitForSeconds(0.2f);
        // presentation_canvas.AddComponent<Rigidbody2D>();
        audio.clip = clips[1];
        audio.Play();
        for (int i = 0; i < letters.Length; i++)
        {
            letters[i].AddComponent<BoxCollider2D>();
            letters[i].AddComponent<Rigidbody2D>();
            yield return new WaitForSeconds(0.15f);
        }
        
        yield return new WaitForSeconds(0.8f);
        audio.clip = clips[2];
        audio.Play();
        yield return new WaitForSeconds(0.5f);
        fini = true;
        run = true;
        h = 1f;
        audio.loop = true;
        audio.clip = clips[3];
        audio.Play();
        CanvasGroup cg = presentation_canvas.GetComponent<CanvasGroup>();
        yield return new WaitForSeconds(1.5f);
        
        audio.loop = false;
        while(compt<10)
        {
            cg.alpha = 1;
            yield return new WaitForSeconds(0.05f);
            cg.alpha = 0;
            yield return new WaitForSeconds(0.05f);
            compt++;
        }
        SceneManager.LoadScene(1);

    }
}
