﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cutscene01 : MonoBehaviour {
    public Sprite[] sprites;
    public Image img;
    public Text nom;
    public Text diag;
    private float fade_speed;
    private float dial_speed;
    private bool wait_diag = false;
    private bool wait_effet = false;
    private string dial = "";
    private int id_sprite;
    private string nom_perso="";
	// Use this for initialization
	void Start () {
        fade_speed = .15f;
        dial_speed = .06f;
        StartCoroutine(Effet());
        StartCoroutine(Dialogue(dial_speed));
        StartCoroutine(Cutscene());
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    private IEnumerator Cutscene()
    {
        change(0, "Saam");
        yield return new WaitForSeconds(fade_speed);

        nv_dialogue("Où suis-je ?");
        yield return new WaitUntil(() => !wait_diag);

        nv_dialogue("Quel est cet endroit ?");
        yield return new WaitUntil(() => !wait_diag);

        nv_dialogue("Cet endroit est inquiétant !");
        yield return new WaitUntil(() => !wait_diag);

        change(1, "Saam");
        yield return new WaitForSeconds(fade_speed);

        nv_dialogue("J'ai changé");
        yield return new WaitUntil(() => !wait_diag);

    }

    private IEnumerator Dialogue(float vitesse)
    {
        while (true)
        {
            yield return new WaitUntil(() => wait_diag);
            diag.CrossFadeAlpha(1, fade_speed, false);
            
            for (int i = 0; i < dial.Length; i++)
            {
                diag.text += dial[i];
                yield return new WaitForSeconds(vitesse);
            }
            yield return new WaitUntil(() => Input.GetKeyDown("return"));
            
            diag.CrossFadeAlpha(0, fade_speed, false);
            yield return new WaitForSeconds(fade_speed);
            wait_diag = false;
            diag.text = "";
        }
    }

    private IEnumerator Effet()
    {
        while (true)
        {
            yield return new WaitUntil(() => wait_effet);
            effet_fade(0, fade_speed);
            yield return new WaitForSeconds(fade_speed);
            img.sprite = sprites[id_sprite];
            nom.text = nom_perso;
            effet_fade(1, fade_speed);
            yield return new WaitForSeconds(fade_speed);
            wait_effet = false;
        }
    }

 
    private void change(int index,string txt_nom)
    {
        wait_effet = true;
        id_sprite = index;
        nom_perso = txt_nom;
        
    }

    private void nv_dialogue(string s)
    {
        dial = s;
        wait_diag = true;
    }

    private void effet_fade(int alpha,float vitesse)
    {

        img.CrossFadeAlpha(alpha, vitesse, false);
        nom.CrossFadeAlpha(alpha, vitesse, false);
        
    }
}
