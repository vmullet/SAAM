﻿using UnityEngine;
using System.Collections;

public class LetterCollision : MonoBehaviour {

    //Vérification de la collision
    void OnTriggerEnter2D(Collider2D collisionObjet)
    {
        //Si le sol entre en collision avec un objet lettre
        //Debug.Log(collisionObjet.name.Substring(0, 6).ToString());
        if (collisionObjet.name.ToString().Equals("grd"))
        {
            //On supprime cet objet, puique le joueur ne l'a pas attrapé
            Destroy(this);
        }

          
    }
  
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
