﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using Assets;

public class DestroyObjectAndAddPoint : MonoBehaviour {
    public static string trans_score;
    public int myScore = 0;
    public string mot = "caca";
    public int lettreMotFound = 0;
    public bool endLevel = false;
    public Text lbl_score;
    public Text lbl_mot;
    public Text lbl_found;
    public GameObject spwaner;
    private new AudioSource audio;
    public AudioClip[] a_clips;
    public GameObject eventSystem;
    // Use this for initialization
    void Start()
    {
        lbl_score.text = "Score : " + myScore;
        lbl_mot.text = "Mot : " + mot;
        lbl_found.text = "Mot formé : ";
    }


    //Vérification de la collision
    void OnTriggerEnter2D(Collider2D collisionObjet){
        audio = GetComponent<AudioSource>();
        //On récupère la lettre de l'objet entré en collision
        char lettreObject = '_';
        if(collisionObjet.name.Substring(0,6).ToString().Equals("letter"))
        {
            lettreObject = char.ToLower(collisionObjet.name[6]);
        }
        
        
        Debug.Log(lettreObject);

        //Si la partie n'est pas déjà finie
        if(!endLevel)
        {
            //Si la lettre est exacte
            if (mot[lettreMotFound] == lettreObject)
            {
                //On augmente le score
                myScore += 5;
                lbl_score.text = "Score : " + myScore;
                lbl_found.text += lettreObject;
                Debug.Log("Mon score est de : " + myScore);
                jouer_son("Success_block");
                lettreMotFound++;

                if (lettreMotFound >= mot.Length)
                {
                    //Fin du niveau => Sauvegarde => Changement Scène
                    endLevel = true;
                    lbl_score.text = "Score fin de manche : " + myScore;
                    jouer_son("Success");
                    Debug.Log("Fin de la partie");
                    Destroy(spwaner);
                    StartCoroutine(Fin_Partie());
                }

                //On supprime l'objet de la scène
                Destroy(collisionObjet.gameObject);
            }
            else
            {
                //On diminue le score
                myScore -= 2;
                lbl_score.text = "Score : " + myScore;
                Debug.Log("Mon score est de : " + myScore);
                jouer_son("Fail_block");
                //On supprime l'objet de la scène
                Destroy(collisionObjet.gameObject);
            }
        }   
    }


	
	// Update is called once per frame
	void Update () {
       
        //lbl_score.text = "Score : " + myScore;
    }


    public void jouer_son(string mode)
    {
        int i = -1; //Position du son dans le tableau de sons
        switch (mode) //Selon l'interaction
        {
            case "Success":
                i = 0; 
                break;
            case "Fail":
                i = 1;
                break;
            case "Success_block":
                i = 2;
                break;
            case "Fail_block":
                i = 3;
                break;

        }
        audio.clip = a_clips[i]; //association du son au lecteur audio
        audio.Play(); //Lecture du son
    }

    private IEnumerator Fin_Partie()
    {
        string url = "http://" + VariableGlobale.ip_serveur + "/saam/nv_partie.php";
        WWWForm form = new WWWForm(); //Création formulaire
        form.AddField("score", myScore); //Ajout des données
        form.AddField("id_niveau", "1"); //Ajout des données
        form.AddField("id_joueur", VariableGlobale.id_joueur); //Ajout des données

        WWW w = new WWW(url, form); //Début téléchargement
        yield return w;        //On attend la fin du téléchargement (=pause)
        VariableGlobale.trans_score = myScore + "";
        eventSystem.GetComponent<Fin_Partie>().enabled = true;
        yield return null;
    }

}
