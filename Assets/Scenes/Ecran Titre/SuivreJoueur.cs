﻿using UnityEngine;
using System.Collections;

public class SuivreJoueur : MonoBehaviour
{

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;
    public new Camera camera;
    float old_y;

        void start()
    {
       old_y = transform.position.y;
       camera = GetComponent<Camera>();
        
    }
    void Update()
    {
        if (target)
        {
            Vector3 point = camera.WorldToViewportPoint(target.position); //Position de la cible
            Vector3 delta = target.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.1f, point.z)); //Delta pour la largeur de la caméra (gauche + droit) , la cible est au centre
            Vector3 destination = transform.position+delta; //Position de la cible incrémenté d'un décalage
            if (destination.y != old_y) //Pour le démarrage du jeu (caméra qui sort de l'écran)
                destination.y = old_y;
            if (destination.x < 7) //Caméra qui sort de l'écran
                destination.x = 7;
            if (destination.x > 46) //Caméra qui sort de l'écran
                destination.x = 46;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime); //Application de la position de la caméra
        }

    }
}