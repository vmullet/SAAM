﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class PlayerControl : MonoBehaviour {
    public float h=1f;
    public float velocity;
    bool pr_slide = false;
    public bool jump = false;
    public bool run = true;
    public bool facingRight = true;

    public float moveForce = 365f;          // Amount of force added to move the player left and right.
    public float maxSpeed = 5f;             // The fastest the player can travel in the x axis.
    
    public float jumpForce = 180f;         // Amount of force added when the player jumps.
   
    private bool grounded = false;
    private bool wall = false;

    private Animator anim;
    private Transform groundCheck;
    private Transform wallCheck;
    
	// Use this for initialization
	void Start () {
        groundCheck = transform.Find("groundCheck");
        wallCheck = transform.Find("wallCheck");
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        wall = Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("Wall"));
        
        if (grounded)
        {
            anim.SetTrigger("StopJumping");
            if (pr_slide)
            {
                if (velocity > 0.05 || velocity < -0.05)
                {
                      
                        anim.SetTrigger("Slide");
                        pr_slide = false;
                    
                }
            }
        }
        
    }

    void FixedUpdate()
    {
        
         auto_move();
        //h= Input.GetAxis("Horizontal");
        

        if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);
            velocity = GetComponent<Rigidbody2D>().velocity.x;
        }

        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
            velocity = GetComponent<Rigidbody2D>().velocity.x;
        }
        
        if (h > 0 && !facingRight)
           Tourne();
        else if (h < 0 && facingRight)
            Tourne();

        if (!run)
        {
            anim.SetTrigger("StopRunning");
        }
        if (jump)
        {
            anim.CrossFade("jump",0f);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
            jump = false;
            pr_slide = true;
        }
        
    }
        void Tourne()
        {

        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void auto_move()
    {
        if (grounded) //Si touche le sol
        {
            run = true; //booléen qui déclenche une force qui fait avancer le personnage
            
            anim.SetTrigger("Run"); //démarrage de l'animation
            if (transform.position.x > -1.5f && transform.position.x < 1.5f) //Saut prévu à cet emplacement
            {    
                    run = false; //Arret de courir
                    jump = true;   //Début du saut 
            }

            if (wall) //Si touche un mur
            {
                h = -h; //Inversion de la force gauche si droite ou droite si gauche
                Tourne(); //Le personnage tourne
            }
        }
        
    }

   

    
}
