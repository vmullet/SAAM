﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using Assets;
using UnityEngine.SceneManagement;

public class TriggerUI : MonoBehaviour
{

    #region Variables

    public InputField[] form_inscription;
    public InputField[] form_login;
    public GameObject[] checkers;
    public Text[] msg_checkers;
    public Sprite[] poss_infos;
    public bool[] etat_checkers;
    public Canvas[] all_canvas;
    public Canvas inscription_canvas;
    public Canvas login_canvas;
    public Canvas msg_canvas;
    public Canvas sel_level;
    public Button btn_inscription;
    public Button btn_retour;
    public Button btn_inscription_valider;
    public InputField confirm_pass;
    public new CanvasGroup renderer;
    private GameObject checker;
    public Sprite success;
    public Sprite fail;
    public GameObject action_cours;
    private Animator anim_cours;
    public Text msg_info;
    public GameObject sprite_msg;
    public Dropdown select_matiere;
    public Dropdown select_niveau;
    public AudioClip s_audio;
    private Text lbl_checker;
    public bool waiter;
    public bool ok;
    private CanvasGroup cg_msg_box;
    private CanvasGroup cg_login_canvas;
    private CanvasGroup cg_inscription_canvas;
    private bool deja_chg = false;
    public Text consigne_titre;
    public Text consigne_infos;
    public Canvas press_canvas;
    public Text press_text;
    private bool press = false;
    private float time_press = 0.0f;
    private float time_elapsed;
    private bool blink;
    public float limite_blink;
    #endregion

    #region Initialisation
    void Start()
    {
        waiter = false;
        ok = false;
        anim_cours = action_cours.GetComponent<Animator>();
        cg_msg_box = msg_canvas.GetComponent<CanvasGroup>();
        cg_inscription_canvas = inscription_canvas.GetComponent<CanvasGroup>();
        cg_login_canvas = login_canvas.GetComponent<CanvasGroup>();
        select_matiere.value = 0;
        Get_Niveau_Matiere();
        if (VariableGlobale.logged)
        {
            login_canvas.enabled = false;
            sel_level.enabled = true;
        }

#if (UNITY_ANDROID || UNITY_IPHONE)
        {
            press_text.text = "Appuyez sur l'écran";
        }
#endif
        StartCoroutine(Blink());
    }

    void Update()
    {

        if (Input.anyKey)
        {
            if (blink)
            {
                if (!VariableGlobale.logged)
                    switch_mode(press_canvas, login_canvas, "disparaitre", "apparaitre");
                else
                    switch_mode(press_canvas, sel_level, "disparaitre", "apparaitre");
                press_canvas.enabled = false;
            }
            time_press = Time.time;
            press = true;

        }


        if (time_press != 0.0f)
        {
            time_elapsed = Time.time - time_press;
            if (time_elapsed > limite_blink)
            {

                press = false;
                time_press = 0.0f;
                if (!blink)
                {
                    switch_mode(GetCanvasActif(), press_canvas, "disparaitre", "apparaitre");
                    StartCoroutine(Blink());
                }


            }
        }
    }
    #endregion

    #region Gestion_Clic
    public void OnClicked(Button btn)
    {
        string nom = btn.name; //nom du bouton cliqué
        switch (nom)
        {
            case "btn_login":
                if (verif_champ(form_login)) //si tous les champs sont remplis
                {
                    StartCoroutine(Coroutine_log()); //Verification en assynchrone
                    StartCoroutine(Wait("btn_login")); //Traitement du résultat en assynchrone
                }
                else
                {
                    cg_login_canvas.interactable = false; //désactivation des interactions avec le canvas
                    affiche("Mince, tu as oublié \n de remplir un champ", "erreur"); //affichage au premier plan de la fenêtre de message
                }

                break;

            case "btn_inscription":

                StartCoroutine(Coroutine_internet());
                StartCoroutine(Wait("btn_inscription"));
                break;

            case "btn_valider_inscription":

                if (verif_champ(form_inscription))
                {
                    verif_checker("pseudo");
                    verif_checker("mail");
                    verif_checker("mdp");
                    if (valid_checker())
                    {
                        StartCoroutine(Coroutine_reg());
                        StartCoroutine(Wait("btn_valider_inscription"));
                    }
                    else
                    {
                        cg_inscription_canvas.interactable = false;
                        affiche("Ooops, regarde bien,\n tu as fait des erreurs", "erreur");
                    }

                }
                else
                {
                    cg_inscription_canvas.interactable = false;
                    affiche("Mince, tu n'as pas \n tout rempli", "erreur");
                }

                break;

            case "btn_retour":
                reset_checkers();
                reset_champ("form_inscription");
                reset_champ("msg_checkers");
                switch_mode(inscription_canvas, login_canvas, "disparaitre", "apparaitre");
                break;

            case "btn_ok_msg_box":

                StartCoroutine(Fade(msg_canvas, "disparaitre"));
                cg_login_canvas.interactable = true;
                cg_inscription_canvas.interactable = true;
                Destroy((msg_canvas.GetComponent("GUIEffects") as MonoBehaviour));
                break;

            case "btn_deconnexion":
                VariableGlobale.logged = false;
                VariableGlobale.pseudo = "";
                VariableGlobale.id_joueur = "";
                switch_mode(sel_level, login_canvas, "disparaitre", "apparaitre");
                cg_login_canvas.interactable = true;
                break;

            case "btn_start_jouer":
                StartCoroutine(change_niveau());
                break;

            default:
                break;

        }
    }
    #endregion

    #region Evenementiel

    public void OnEndEdit(InputField input)
    {
        if (input.text.Length != 0)
        {
            string nom = input.name;
            string action = "";
            int nb_chp = -1;

            string message_ok = "";
            string message_pas_ok = "";
            switch (nom)
            {
                case "txt_pseudo":
                    action = "pseudo";
                    nb_chp = 0;
                    lbl_checker = msg_checkers[0];
                    message_ok = "Pseudo Disponible";
                    message_pas_ok = "Pseudo Indisponible";
                    StartCoroutine(Coroutine_checker(action, nb_chp, lbl_checker, message_ok, message_pas_ok));
                    break;

                case "txt_mail":
                    action = "mail";
                    nb_chp = 3;
                    lbl_checker = msg_checkers[1];
                    message_ok = "E-Mail valide";
                    message_pas_ok = "E-Mail invalide";
                    StartCoroutine(Coroutine_checker(action, nb_chp, lbl_checker, message_ok, message_pas_ok));
                    break;

                case "txt_mdp1":
                case "txt_mdp2":
                    checker = GameObject.Find("verif_mdp");
                    Animator anim = checker.GetComponent<Animator>();


                    lbl_checker = msg_checkers[2];
                    if (form_inscription[4].text.Length != 0 && form_inscription[5].text.Length != 0)
                    {
                        anim.CrossFade("checker", 0f);
                        if (form_inscription[4].text == form_inscription[5].text)
                        {
                            anim.CrossFade("success", 0f);
                            lbl_checker.text = "Mot de passe identiques";
                        }
                        else
                        {
                            anim.CrossFade("fail", 0f);
                            lbl_checker.text = "Mot de passe différents";
                        }
                    }
                    break;

            }

        }
    }

    #endregion

    #region Méthodes

    private void switch_mode(Canvas deb, Canvas fin, string mode_deb, string mode_fin)
    {
        determ_etat(deb, mode_deb);
        StartCoroutine(Fade(deb, mode_deb));

        determ_etat(fin, mode_fin);
        StartCoroutine(Fade(fin, mode_fin));

    }

    private void determ_etat(Canvas c, string md_deb)
    {
        if (md_deb == "disparaitre")
            c.enabled = false;
        else
            c.enabled = true;
    }

    private void reset_checkers()
    {
        for (int i = 0; i < checkers.Length; i++)
        {
            Animator anim = checkers[i].GetComponent<Animator>();
            anim.CrossFade("Rien", 0f);
        }
    }

    private void reset_champ(string action)
    {
        switch (action)
        {
            case "form_inscription":

                for (int i = 0; i < form_inscription.Length; i++)
                    form_inscription[i].text = "";
                break;
            case "form_login":
                for (int i = 0; i < form_login.Length; i++)
                    form_login[i].text = "";
                break;

            case "msg_checkers":
                for (int i = 0; i < msg_checkers.Length; i++)
                    msg_checkers[i].text = "";
                break;
        }
    }

    private void info(string message, string type)
    {
        Image img = sprite_msg.GetComponent<Image>();
        switch (type)
        {
            case "erreur":
                img.sprite = poss_infos[0];
                break;
            case "info":
                img.sprite = poss_infos[1];
                break;
            case "confirmation":
                img.sprite = poss_infos[2];
                break;
        }
        msg_info.text = message;
        cg_msg_box.alpha = 0;

        GameObject obj = GameObject.Find("msg_canvas");
        if (deja_chg)
        {
            MonoBehaviour cp = obj.AddComponent<GUIEffects>() as MonoBehaviour;
        }
        deja_chg = true;
        (msg_canvas.GetComponent("GUIEffects") as MonoBehaviour).enabled = true;
    }

    private void affiche(string message, string type)
    {
        info(message, type); //Affectation du libelle et de l'image correspondant au type
        StartCoroutine(Fade(msg_canvas, "apparaitre")); //Apparition du canvas en fondu
        msg_canvas.enabled = true; //Action de l'interaction avec le canvas
    }

    private bool verif_champ(InputField[] tab_chp)
    {

        for (int i = 0; i < tab_chp.Length; i++)
        {
            if (tab_chp[i].text == "")
                return false;
        }

        return true;
    }

    private void verif_checker(string p_action)
    {

        SpriteRenderer s;
        GameObject g = null;
        int i = -1;
        switch (p_action)
        {
            case "pseudo":
                g = GameObject.Find("verif_pseudo");
                i = 0;
                break;
            case "mail":
                g = GameObject.Find("verif_mail");
                i = 1;
                break;
            case "mdp":
                g = GameObject.Find("verif_mdp");
                i = 2;
                break;
        }
        s = g.GetComponent<SpriteRenderer>();
        if (s.sprite == success)
            etat_checkers[i] = true;
        else
            etat_checkers[i] = false;

    }

    private bool valid_checker()
    {
        bool b = true;
        for (int i = 0; i < etat_checkers.Length; i++)
        {
            if (!etat_checkers[i])
            {
                return false;
            }
        }
        return b;
    }


    private Canvas GetCanvasActif()
    {
        for (int i = 0; i < all_canvas.Length; i++)
        {
            if (all_canvas[i].enabled)
            {
                return all_canvas[i];
            }
        }
        return null;
    }

    public void Get_Niveau_Matiere()
    {
        select_niveau.interactable = false;
        int index = select_matiere.value;

        select_niveau.options.Clear();
        switch (index)
        {
            case 0:
                select_niveau.options.Add(new Dropdown.OptionData { text = "Les Mots de ToutanCarton" });
                break;
            case 1:
                select_niveau.options.Add(new Dropdown.OptionData { text = "Le compte est bon !" });
                select_niveau.options.Add(new Dropdown.OptionData { text = "Pas d'impair !" });
                break;
        }

        select_niveau.interactable = true;
        select_niveau.value = 1;

    }

    public void Recup_Id_Niveau()
    {
        int index_niveau = select_matiere.value + select_niveau.value +1; //récupération de l'index du niveau sélectionné

        Dropdown.OptionData[] ls = new Dropdown.OptionData[select_niveau.options.Count]; //création d'une liste ayant pour taille le nombre de niveaux possibles pour la matière
        select_niveau.options.CopyTo(ls); //copie de cette liste
        string niveau_select = ls[index_niveau - select_matiere.value - 1].text; //récupération du nom du niveau
        switch (niveau_select)
        {
            case "Les Mots de ToutanCarton":
                consigne_titre.text = "Les Mots de ToutanCarton";
                consigne_infos.text = "Dans ce niveau, tu devras reformer un mot en attrapant les blocs des lettres qui le constituent dans l'ordre.";
                break;
            case "Le compte est bon !":
                consigne_titre.text = "Le compte est bon !";
                consigne_infos.text = "Dans ce niveau, tu devras reformer le calcul permettant d'obtenir le nombre demandé.";
                break;
            case "Pas d'impair !":
                consigne_titre.text = "Pas d'impair !";
                consigne_infos.text = "Dans ce niveau, tu devras attraper les blocs des nombres pairs ou impairs. Attention, la consigne changera au cours du temps !!!";
                break;
        }
        ls = null;
        VariableGlobale.niv_chg = index_niveau+1;

    }

    #endregion

    #region Coroutines
    private IEnumerator Coroutine_reg()
    {
        waiter = false;
        ok = false;
        string url_form = "http://" + VariableGlobale.ip_serveur + "/saam/register.php";

        WWWForm form = new WWWForm();
        form.AddField("pseudo", form_inscription[0].text);
        form.AddField("nom", form_inscription[1].text);
        form.AddField("prenom", form_inscription[2].text);
        form.AddField("mail", form_inscription[3].text);
        form.AddField("pass", form_inscription[4].text);
        cg_inscription_canvas.interactable = false;
        WWW w = new WWW(url_form, form);


        anim_cours.SetTrigger("StartConnecting");
        yield return w;
        anim_cours.SetTrigger("StopConnecting");

        if (w.text.Contains("Success"))
        {
            ok = true;
        }
        else
        {
            ok = false;

        }

        yield return null;
        waiter = true;
    }
    private IEnumerator Coroutine_log()
    {
        waiter = false;
        ok = false;
        CanvasGroup c = login_canvas.GetComponent<CanvasGroup>();
        string url_form = "http://" + VariableGlobale.ip_serveur + "/saam/login.php"; //url du php

        WWWForm form = new WWWForm(); //Création d'un objet formulaire
        form.AddField("pseudo", form_login[0].text); //Ajout des données saisies
        form.AddField("pass", form_login[1].text); //Ajout des données saisies
        c.interactable = false;
        WWW w = new WWW(url_form, form); //Envoi du formulaire à l'url
        anim_cours.SetTrigger("StartConnecting");
        yield return w; //Attente avant que le serveur reponde
        anim_cours.SetTrigger("StopConnecting");

        if (w.text.Contains("Success")) //Traitement du résultat (en partie)
        {
            string s = w.text.ToString();
            string id_joueur = s.Substring(s.IndexOf("---") + 3);
            VariableGlobale.id_joueur = id_joueur;

            ok = true;
        }
        else
        {
            ok = false;

        }
        yield return null;
        waiter = true;
    }
    private IEnumerator Coroutine_checker(string p_action, int p_num, Text t, string msg_ok, string msg_pas_ok)
    {
        checker = GameObject.Find("verif_" + p_action);
        Animator anim = checker.GetComponent<Animator>();

        anim.CrossFade("checker", 0f); //Lecture de l'animation de vérification
        string url_form = "http://" + VariableGlobale.ip_serveur + "/saam/check_form.php";

        WWWForm form = new WWWForm();
        form.AddField("action", p_action);
        form.AddField(p_action, form_inscription[p_num].text);

        WWW w = new WWW(url_form, form);

        yield return w;

        if (w.text.Contains("Success"))
        {
            if (w.text.Contains("Already"))
            {
                t.text = "Adresse déjà utilisée";
                anim.SetTrigger("Fail"); //Animation si adresse déjà utilisée
            }
            else
            {
                t.text = msg_ok;
                anim.SetTrigger("Success"); //Animation si texte valide
            }

        }
        else
        {
            anim.SetTrigger("Fail"); //Animation si texte non valide

            t.text = msg_pas_ok;
        }

        yield return null;
    }

    private IEnumerator Coroutine_internet()
    {
        waiter = false;
        ok = false;
        string url_test = "http://" + VariableGlobale.ip_serveur + "/saam/check.php";
        CanvasGroup c = login_canvas.GetComponent<CanvasGroup>();
        c.interactable = false;
        WWW w = new WWW(url_test);
        VariableGlobale.internet = true;
        anim_cours.SetTrigger("StartConnecting");
        yield return w;
        anim_cours.SetTrigger("StopConnecting");
        if (w.text.Contains("Success"))
        {
            ok = true;
        }
        else
        {
            ok = false;
        }
        yield return null;
        waiter = true;

    }

    private IEnumerator Fade(Canvas c, string mode)
    {
        waiter = false; //booléen pour savoir quand c'est terminé(asynchrone)
        CanvasGroup cg = c.GetComponent<CanvasGroup>(); //Récupération du composant qui gère la transparence

        float currentTime = 0f;
        float duree = 0.50f; //durée de la transition
        float min = 0f; //Initialisation
        float max = 0f; //Initialisation

        switch (mode)
        {
            case "apparaitre":
                min = 0f; //Valeur de départ
                max = 1f; //Valeur de fin

                break;
            case "disparaitre":
                min = 1f; //Valeur de départ
                max = 0f; //Valeur de fin
                break;
        }
        c.enabled = true;
        while (currentTime < duree)
        {
            cg.alpha = Mathf.Lerp(min, max, currentTime / duree); //Calcul de la transparence selon le temps,la vitesse
            currentTime += Time.deltaTime; //Durée depuis le début de la transition
            yield return null;
        }
        if (mode == "disparaitre")
            c.enabled = false; //Désactiviation de l'élément en mémoire puisqu'il disparaît graphiquement

        yield return null;

        waiter = true;
    }

    private IEnumerator change_niveau()
    {
        CanvasGroup c = sel_level.GetComponent<CanvasGroup>();
        c.interactable = false;
        GameObject bg = GameObject.Find("background");
        AudioSource s = bg.GetComponent<AudioSource>();
        s.loop = false;
        if (s.isPlaying)
            s.Stop();
        s.clip = s_audio;
        s.Play();
        anim_cours.SetTrigger("StartLoading");
        while (s.isPlaying)
        {

            yield return new WaitForSeconds(0.002f);

        }
        float fadeTime = GameObject.Find("loader_level").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        anim_cours.SetTrigger("StopLoading");
        c.interactable = true;

        SceneManager.LoadScene(VariableGlobale.niv_chg);
    }

    private IEnumerator Wait(string action)
    {
        while (!waiter)
        {
            yield return new WaitForSeconds(0.1f);
        }
        switch (action)
        {
            case "btn_inscription":
                if (ok)
                {
                    cg_login_canvas.interactable = true;
                    switch_mode(login_canvas, inscription_canvas, "disparaitre", "apparaitre");
                }
                else
                {
                    info("Ooops, \nje ne peux pas\n me connecter", "erreur");
                    VariableGlobale.internet = false;
                }
                break;
            case "btn_login":
                if (ok)
                {
                    VariableGlobale.logged = true;
                    VariableGlobale.pseudo = form_login[0].text;
                    StartCoroutine(Fade(login_canvas, "disparaitre"));
                    yield return null;
                    reset_champ("form_login");
                    StartCoroutine(Fade(sel_level, "apparaitre"));
                }
                else
                {
                    info("Zut, je ne \n te reconnais pas", "erreur");
                    StartCoroutine(Fade(msg_canvas, "apparaitre"));
                    msg_canvas.enabled = true;
                }
                break;

            case "btn_valider_inscription":
                if (ok)
                {
                    VariableGlobale.logged = true;
                    VariableGlobale.pseudo = form_inscription[0].text;
                    affiche("Bravo, tu peux \ncommencer à jouer !", "confirmation");
                    switch_mode(inscription_canvas, sel_level, "disparaitre", "apparaitre");
                    yield return null;
                    reset_champ("form_inscription");
                    reset_checkers();
                }
                else
                {
                    affiche("Aaargh, je ne peux \n pas t'enregistrer !", "erreur");
                }
                break;
        }

    }

    private IEnumerator Blink()
    {
        blink = true;
        while (!press)
        {
            StartCoroutine(Fade(press_canvas, "disparaitre"));
            while (!waiter)
            {
                yield return new WaitForSeconds(0.2f);
            }
            StartCoroutine(Fade(press_canvas, "apparaitre"));
            while (!waiter)
            {
                yield return new WaitForSeconds(0.2f);
            }
            yield return new WaitForSeconds(0.1f);
        }
        blink = false;


    }
    #endregion


}
