﻿using UnityEngine;
using System.Collections;

public class Transition : MonoBehaviour {
    SpriteRenderer s;
    public float minimum = 0.0f;
    public float maximum = 1f;
    public float duration = 5.0f;
    public float startTime;
    public float alpha;
    public Sprite[] diaporama;
    public bool reset_temps;
    public string mode;
    public int cpt;
    // Use this for initialization
    void Start () {
        s = GetComponent<SpriteRenderer>();
        StartCoroutine("Fade_Out");
    }
	
	// Update is called once per frame
	void Update () {
    
    }
   
    IEnumerator Fade_Out()
    {
        float duration = 5f; //5 secondes durée de la transition
        float currentTime = 0f; //Temps actuel (pour réinitialiser quand on change de mode)
        reset_temps = false; //booleen qui gère la réinitialisation du temps
        bool change = false; //booleen qui gère le changement de mode (apparaitre ou disparaitre)
        mode = "Apparaitre";
        cpt = 0;
        while (currentTime < duration)
        {
            if (currentTime > duration-1) //On prépare le changement 1 seconde avant
                change = true;
            if (mode.Equals("Apparaitre"))
            alpha = Mathf.Lerp(0.05f, 1f, currentTime / duration); //On va de 0.05 à 1
            if (mode.Equals("Disparaitre"))
                alpha = Mathf.Lerp(0.96f, 0.02f, currentTime / duration); //On va de 0.96 à 0.02
            if (alpha > 0.98f)
            {
                mode = "Disparaitre";
                if (!reset_temps) //Si le temps n'a pas encore été réinitialisé
                {
                    currentTime = 0f; //Réinitialisation du temps
                    reset_temps = true;
                }            
            }
            if (alpha < 0.03f)
            {
                mode = "Apparaitre";

                if (!reset_temps)
                {
                    currentTime = 0f;

                    reset_temps = true;
                    cpt = cpt + 1;

                if (cpt > 5)
                    cpt = 0;

                s.sprite = diaporama[cpt];
                            
                }   
            }
            if (change)
            {
                reset_temps = false;
                change = false;
            }
            s.color = new Color(s.color.r, s.color.g, s.color.b, alpha);
            currentTime += Time.deltaTime;
            yield return null;
        }
        yield break;
    }
 

    }
