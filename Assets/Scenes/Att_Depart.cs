﻿using UnityEngine;
using System.Collections;

public class Att_Depart : MonoBehaviour {
    public Canvas Control_canvas;
	// Use this for initialization
	void Start () {
#if (UNITY_ANDROID || UNITY_IPHONE)
        {
            Control_canvas.enabled=true;
        }

#endif
        StartCoroutine(att_spawn());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator att_spawn()
    {
        while (Init_Niveau.wait)
        {
            yield return new WaitForSeconds(0.1f);
        }
        GetComponent<SpawnObjects>().enabled = true;
        yield return null;
    }
}
